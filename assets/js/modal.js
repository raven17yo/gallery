$( document ).ready(function() {

  $(document).on('click', '.project', openModal); 
    
  $(document).on('click', '.modal-close', closeModal);
  
  function closeModal(){
    $(".modal").removeClass("opened")
    $(".slider-section").removeClass("opened")
  }

  function openModal(){
    var id = parseInt($(this).attr('id'))
    $(".slider-section").removeClass("opened")
    $(".modal").addClass("opened")
    $(".project-images").text('')
    $(".project-title").text(projectsData[id].name)
    $(".project-date").text(projectsData[id].date)
    $(".project-category").text("Kategoria: " + projectsData[id].category)
    $(".project-adres").text(projectsData[id].adres)
    $(".project-client").text(projectsData[id].client)
    $(".project-adres").prop("href", "http://" + projectsData[id].adres)
    for(i = 0; i < projectsData[id].projectImages.length; i++){
      $(".project-images").append("<figure class='project-img "+ id +"'><img src="+ projectsData[id].projectImages[i] +"></figure>")
    }
  }

});