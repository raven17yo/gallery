var projectsData = [{
  id: 0,
  name: 'Szablon #1',
  date: '15/12/2016',
  category: 'Aplikacje Mobilne',
  adres: "www.google.pl",
  client: "Google Company",
  miniImg: 'assets/images/project1-mini.jpg',
  projectImages: ['assets/images/project1-img1.jpg', 'assets/images/project1-img2.jpg', 'assets/images/project1-img3.jpg']
},
{
  id: 1,
  name: 'Szablon #2',
  date: '16/25/2016',
  category: 'Strony Internetowe',
  adres: "www.google.pl",
  client: "Google Company",
  miniImg: 'assets/images/project2-mini.jpg',
  projectImages: ['assets/images/project2-img1.jpg', 'assets/images/project2-img2.jpg', 'assets/images/project2-img3.jpg']
},
{
  id: 2,
  name: 'Szablon #5',
  date: '16/25/2016',
  category: 'Aplikacje Mobilne',
  adres: "www.google.pl",
  client: "Google Company",
  miniImg: 'assets/images/project3-mini.jpg',
  projectImages: ['assets/images/project3-img1.jpg', 'assets/images/project3-img2.jpg', 'assets/images/project3-img3.jpg']
},
{
  id: 3,
  name: 'Szablon #3',
  date: '17/25/2016',
  category: 'Strony Internetowe',
  adres: "www.google.pl",
  client: "Google Company",
  miniImg: 'assets/images/project4-mini.jpg',
  projectImages: ['assets/images/project4-img1.jpg', 'assets/images/project4-img2.jpg', 'assets/images/project2-img3.jpg']
},{
  id: 4,
  name: 'Szablon #4',
  date: '14/45/2016',
  category: 'Aplikacje Mobilne',
  adres: "www.google.pl",
  client: "Google Company",
  miniImg: 'assets/images/project5-mini.jpg',
  projectImages: ['assets/images/project5-img1.jpg', 'assets/images/project5-img2.jpg', 'assets/images/project5-img3.jpg']
}]