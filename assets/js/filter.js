$( document ).ready(function() {

  $(document).on('click', '.btn-filter', filterByCategory);
  
  // function filterFirstThree(){
  //   firstThreeProjects = projectsData.slice(0, 3)
  //   projectsLoad('.main-projects', firstThreeProjects)
  // }
  
  function filterByCategory(){
    var category = $(this).attr('filterBy')
    if(!category){
      projectFiltered = projectsData
    }
    else if(category){
      projectFiltered = projectsData.filter(function(el){
        return el.category === category
      })        
    }
    projectsLoad('.realizations', projectFiltered)
  }

  function projectsLoad(section, filteredProjects){
    $(section).text('');
    for(i = 0; i < filteredProjects.length; i++){
      $(section).append('<a id="'+ filteredProjects[i].id +'" class="project active"><span class="project-detail"><div class="project-info"><h4>' + filteredProjects[i].name + '</h4><p>' + filteredProjects[i].category + '</p></div><button class="project-btn-link">Powiększ</button></span><figure><img src="'+ filteredProjects[i].miniImg +'" alt="project photo" /></figure></a>')
    }
  }
  
  // filterFirstThree()
  filterByCategory()
});